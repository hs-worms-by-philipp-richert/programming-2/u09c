// Aufgabe u09c
// Subscript operator overloading - simulieren von Arrayverhalten
// Philipp Richert
// 08.12.2022

#include <iostream>
#include "cJukebox.h"

using namespace std;

int main() {
	cJukebox jukebox1;
	int input;
	int i;

	do {
		cout << "Titelnummer eingeben: ";
		cin >> input;
		i = jukebox1[input];

		cout << "Subskriptionsergebnis: " << i << endl << endl;
	} while (input != 222);

	return 0;
}