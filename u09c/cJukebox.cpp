#include "cJukebox.h"

// overload subscript operator
int& cJukebox::operator [] (const int& idx) {
	static int rtrnVal;

	if (idx < 1 || idx > 300)	// handle out-of-range index
		cerr << "Error: Array index out of range, must be between 1-300" << endl;

	// valid index
	if (idx >= 1 && idx <= 100) {
		cout << "Lang leben Twist, Boogie, RocknRoll!" << endl;
		rtrnVal = idx * 2;
	}
	else if (idx >= 101 && idx <= 200) {
		cout << "Melancholisch schauen und gelegentlich seufzen" << endl;
		rtrnVal = idx * 13 - 100;
	}
	else if (idx >= 201 && idx <= 300 && idx != 222) {	// exclude 222, it's implemented down below
		cout << "Raven bis zum Morgengrauen" << endl;
		rtrnVal = idx * 717 % 11;
	}
	else if (idx == 222) {
		cout << "It's time to say goodbye" << endl;
		rtrnVal = -1;
	}

	return rtrnVal;
}